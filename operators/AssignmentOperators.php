<?php
$x = 10;  
echo $x; //10


echo "<hr>";

$y = 100;  
echo $y; //100
echo "<hr>";

//Addition
$z = 20;  
$z += 100;

echo $z; //120

echo "<hr>";

$a = 20;  
$a += 10;

echo $a; //30

echo "<hr>";
// Subtraction	

$x = 50;
$x -= 30;

echo $x; //20

echo "<hr>";
$x = 30;
$x -= 30;

echo $x; //0
echo "<hr>";
//Multiplication

$x = 10;  
$y = 6;

echo $x * $y; //60

echo "<hr>";


$x = 50;  
$y = 6;

echo $x * $y; //300

echo "<hr>";
//Division

$x = 10;
$x /= 5;

echo $x;//2

echo "<hr>";

$x = 5;
$x /= 10;

echo $x;//0.5
echo "<hr>";
//Modulus

$x = 15;
$x %= 4;

echo $x;//3

echo "<hr>";

$x = 12;
$x %= 4;

echo $x;//0

?>