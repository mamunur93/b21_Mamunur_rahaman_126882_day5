<?php

//Pre-increment	

$x = 10;  
echo ++$x; //11
echo "<hr>";

$x = 11;  
echo ++$x; //12
echo "<hr>";

//Post-increment
echo "<hr>";
$x = 10;  
echo $x++; //10

echo "<hr>";
echo $x++; //11

echo "<hr>";
$x = 15;  
echo $x++; //15

echo "<hr>";
echo $x++; //16
