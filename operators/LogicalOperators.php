<?php 

// And

$x = 100;  
$y = 50;

if ($x == 100 and $y == 50) {
    echo "Success";
} 

else {
	
	 echo " not Success";
} // Success

echo "<hr>";


$x = 88;  
$y = 50;

if ($x == 100 and $y == 50) {
    echo "Success";
} // Success
else {
	
	 echo " not Success";
}  // not Success

// Or

echo "<hr>";

$x = 100;  
$y = 50;

if ($x == 100 or $y == 80) {
    echo "Success";
}

else {
	
	 echo " not Success";
}  // Success

echo "<hr>";

$x = 50;  
$y = 50;

if ($x == 100 or $y == 80) {
    echo "Success";
}

else {
	
	 echo " not Success";
}  // not Success

//Xor

echo "<hr>";

$x = 100;  
$y = 50;

if ($x == 100 xor $y == 80) {
    echo "Success";
}
else {
	
	 echo " not Success";
} //Success

echo "<hr>";

$x = 100;  
$y = 100;

if ($x == 100 xor $y == 80) {
    echo "Success";
}
else {
	
	 echo " not Success";
} //Success

//And

echo "<hr>";

$x = 100;  
$y = 50;

if ($x == 100 && $y == 50) {
    echo "Success";
}

else {
	
	 echo " not Success";
} //Success


echo "<hr>";

$x = 80;  
$y = 50;

if ($x == 100 && $y == 50) {
    echo "Success";
}

else {
	
	 echo " not Success";
} //not Success

//Not
echo "<hr>";
$x = 100;  

if ($x !== 90) {
    echo "Success";
}

else {
	
	 echo " not Success";
} //Success

echo "<hr>";
$x = 1000;  

if ($x !== 90) {
    echo "Success";
}

else {
	
	 echo " not Success";
} //  Success