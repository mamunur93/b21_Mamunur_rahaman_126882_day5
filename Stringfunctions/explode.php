<?php
$str = 'one,two,three,four';

// zero limit
print_r(explode(',',$str,0)); // Array ( [0] => one,two,three,four ) 
echo "<hr>";

// positive limit
print_r(explode(',',$str,2)); //Array ( [0] => one [1] => two,three,four ) 
echo "<hr>";
// negative limit 
print_r(explode(',',$str,-1)); //Array ( [0] => one [1] => two [2] => three )
?>