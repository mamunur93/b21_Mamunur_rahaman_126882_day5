<?php
$str = "This is nice";
echo strlen($str)."<hr>"; // 12
echo substr_count($str,"is")."<hr>"; // 2
echo substr_count($str,"is",2)."<hr>"; // 2
echo substr_count($str,"is",3)."<hr>"; // 1 
echo substr_count($str,"is",3,3)."<hr>"; // 0
?>