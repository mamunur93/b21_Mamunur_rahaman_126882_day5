<?php

//Equal
$x = 100;  
$y = "100";

var_dump($x == $y); // bool(true)

echo "<hr>";

$x = 10;  
$y = "100";

var_dump($x == $y); //bool(false)

echo "<hr>";

//Identical
$x = 100;  
$y = 100;

var_dump($x === $y); // bool(true)

echo "<hr>";

$x = 10;  
$y = 100;

var_dump($x === $y); // bool(false)


echo "<hr>";
//Not equal

$x = 100;  
$y = "100";

var_dump($x != $y); // bool(false)

echo "<hr>";


$x = 10;  
$y = "100";

var_dump($x != $y); // bool(true)

//Not equal
echo "<hr>";

$x = 100;  
$y = "100";

var_dump($x <> $y); // bool(false)

echo "<hr>";

$x = 100;  
$y = "10";

var_dump($x <> $y); // bool(true)

//Not identical	

echo "<hr>";

$x = 100;  
$y = "100";

var_dump($x !== $y); // bool(true)
echo "<hr>";
echo $x !== $y; //1

//Greater than

echo "<hr>";

$x = 100;
$y = 50;

var_dump($x > $y); // bool(true)

echo "<hr>";

$x = 10;
$y = 50;

var_dump($x > $y); // bool(false)

//Less than
echo "<hr>";
$x = 10;
$y = 50;

var_dump($x < $y);//bool(true)

echo "<hr>";
$x = 10;
$y = 10;

var_dump($x < $y);//bool(false)
?> 